﻿using UnityEngine;
using UnityEngine.UI;

public class MatchResultShower : MonoBehaviour
{
    public Text roundPlayerScore;
    public Text roundAIScore;
    public Text matchPlayerScore;
    public Text matchAIScore;
    public Text totalPlayerScore;
    public Text totalAIScore;

    public void Start()
    {
        var score = GameController.GetGameScore();
        
        roundPlayerScore.text = score.RoundPlayerScore.ToString();
        roundAIScore.text = score.RoundAIScore.ToString();
        matchPlayerScore.text = score.MatchPlayerScore.ToString();
        matchAIScore.text = score.MatchAIScore.ToString();
        totalPlayerScore.text = score.TotalPlayerScore.ToString();
        totalAIScore.text = score.TotalAIScore.ToString();
    }
}