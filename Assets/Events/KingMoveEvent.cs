using System.Collections.Generic;

namespace Events
{
    public class KingMoveEvent
    {
        private List<IKingMoveListener> _listeners = new List<IKingMoveListener>();

        public void Subscribe(IKingMoveListener listener)
        {
            _listeners.Add(listener);
        }

        public void UnsubScribe(IKingMoveListener listener)
        {
            _listeners.Remove(listener);
        }

        public void Publish(RelativeCoordinates coordinates)
        {
            foreach (var listener in _listeners)
            {
                listener.ProcessMoveFinish(coordinates);
            }
        }

        public interface IKingMoveListener
        {
            void ProcessMoveFinish(RelativeCoordinates coordinates);
        }
    }
}