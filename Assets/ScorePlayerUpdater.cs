﻿using TMPro;
using UnityEngine;

public class ScorePlayerUpdater : MonoBehaviour
{
    public void Start()
    {
        GameController.GETInstance().ScorePlayerUpdater = this;
    }

    public void UpdateScore(int newScore)
    {
        GetComponent<TextMeshProUGUI>().text = newScore.ToString();
    }
}