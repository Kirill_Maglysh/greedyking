﻿using TMPro;
using UnityEngine;

public class GameInitializer : MonoBehaviour
{
    private const int MAXCellPoint = 51;

    // Максимальное значение случайного количества очков в клетке. 
    // Выбирается с коэффицентом от MAXCellPoint для того, чтобы увеличить количество пустых ячеек. 
    // Все значения которые больше MAXCellPoint будут приравнены к 0.
    private const int CorrectingMAXCellPoint = (int) (MAXCellPoint * 1.8);

    public GameObject lightCell;
    public GameObject darkCell;
    public GameObject artificialIntelligence;

    public void Start()
    {
        var cellPoints = InitChessField();
        GameController.SetCellPoints(cellPoints);
        artificialIntelligence.GetComponent<ArtificialIntelligence>().Initialize(cellPoints);

        if (GameController.GetCurrentPlayer() != PlayerType.ArtificialIntelligence)
        {
            return;
        }

        var initialKingCoordinates = new RelativeCoordinates
        {
            Horizontal = 0, Vertical = 0
        };

        GameController.GETInstance().ArtificialIntelligence
            .MakeMove(initialKingCoordinates);
    }

    private int[,] InitChessField()
    {
        var isDarkCell = false;
        var cellPoints = new int[GameController.FieldSize, GameController.FieldSize];

        for (var horizontal = 0; horizontal < GameController.FieldSize; horizontal++)
        {
            for (var vertical = 0; vertical < GameController.FieldSize; vertical++)
            {
                var cellPoint = horizontal == 0 && vertical == 0 ? 0 : Random.Range(0, CorrectingMAXCellPoint);
                cellPoint = cellPoint < MAXCellPoint ? cellPoint : 0;
                // В последнюю ячейку кладём количество очков, на 1 превышающее максимальное
                cellPoint = horizontal == GameController.FieldSize - 1 && vertical == GameController.FieldSize - 1
                    ? MAXCellPoint
                    : cellPoint;


                cellPoints[horizontal, vertical] = cellPoint;

                var coordinates = new RelativeCoordinates()
                {
                    Horizontal = horizontal,
                    Vertical = vertical
                };

                var newCell = CreateCell(isDarkCell ? darkCell : lightCell, coordinates, cellPoint);
                GameController.GETInstance().KingMoveEvent.Subscribe(newCell.GetComponent<CellClickListener>());
                isDarkCell = !isDarkCell;
            }

            // ReSharper disable once ConditionIsAlwaysTrueOrFalse
            if (GameController.FieldSize % 2 == 0)
            {
                isDarkCell = !isDarkCell;
            }
        }

        Destroy(lightCell);
        Destroy(darkCell);

        return cellPoints;
    }

    private static GameObject CreateCell(GameObject parent, RelativeCoordinates coordinates, int cellPoint)
    {
        var cell = Instantiate(parent, CoordinateTransformer.transform(coordinates), Quaternion.identity);
        var canvasObject = cell.GetComponentInChildren<Canvas>().gameObject;
        canvasObject.GetComponentInChildren<TextMeshProUGUI>().text = cellPoint == 0 ? "" : cellPoint.ToString();

        return cell;
    }
}