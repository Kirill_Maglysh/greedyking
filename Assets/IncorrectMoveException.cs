using System;

public class IncorrectMoveException : Exception
{
    private const string DefaultMessage = "Вы не можете сходить в клетку ";

    public IncorrectMoveException() : base(DefaultMessage)
    {
    }
}