using UnityEngine;

public static class CoordinateTransformer
{
    private static readonly Vector3 StartCell = new Vector3(5f, 0f, 5f);
    private const float CellSide = 10;

    public static Vector3 transform(RelativeCoordinates coordinates)
    {
        var shift = new Vector3(coordinates.Horizontal * CellSide, 0, coordinates.Vertical * CellSide);
        return StartCell + shift;
    }

    public static RelativeCoordinates transform(Vector3 coordinates)
    {
        var diff = coordinates - StartCell;
        return new RelativeCoordinates
        {
            Horizontal = (int) (diff.x / CellSide + 0.5),
            Vertical = (int) (diff.z / CellSide + 0.5)
        };
    }
}