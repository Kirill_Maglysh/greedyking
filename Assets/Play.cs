﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class Play : MonoBehaviour
{
    public void PlayMatch()
    {
        var player = Random.Range(0, 2) == 0 ? PlayerType.LivePlayer : PlayerType.ArtificialIntelligence;
        GameController.InitializeMatch(player);
        SceneManager.LoadScene("MainScene");
    }
    
    public void PlayNextRound()
    {
        GameController.InitializeRound();
        SceneManager.LoadScene("MainScene");
    }
}