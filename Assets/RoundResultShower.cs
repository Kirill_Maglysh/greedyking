﻿using UnityEngine;
using UnityEngine.UI;

public class RoundResultShower : MonoBehaviour
{
    public Text roundNumber;
    public Text playerScore;
    public Text AIScore;

    public void Start()
    {
        var score = GameController.GetGameScore();
        roundNumber.text = score.RoundNumber.ToString();
        playerScore.text = score.RoundPlayerScore.ToString();
        AIScore.text = score.RoundAIScore.ToString();
    }
}