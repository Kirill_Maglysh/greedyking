public class GameScore
{
    public int RoundNumber { get; set; }

    public int MatchAIScore { get; set; }
    public int MatchPlayerScore { get; set; }

    public int RoundAIScore { get; set; }
    public int RoundPlayerScore { get; set; }

    public int TotalAIScore { get; set; }
    public int TotalPlayerScore { get; set; }
}