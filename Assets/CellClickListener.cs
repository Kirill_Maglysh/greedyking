﻿using Events;
using UnityEngine;

public class CellClickListener : MonoBehaviour, KingMoveEvent.IKingMoveListener
{
    public Canvas cellCanvas;

    private RelativeCoordinates _coordinates;

    public void Start()
    {
        _coordinates = CoordinateTransformer.transform(gameObject.transform.position);
    }

    public void OnMouseDown()
    {
        GameController.GETInstance().LivePlayerMove(_coordinates);
    }

    public void ProcessMoveFinish(RelativeCoordinates coordinates)
    {
        if (_coordinates != coordinates)
        {
            return;
        }

        cellCanvas.gameObject.SetActive(false);
        GameController.GETInstance().MoveFinished(coordinates);
    }

    private void OnDestroy()
    {
        GameController.GETInstance().KingMoveEvent.UnsubScribe(this);
    }
}