﻿using TMPro;
using UnityEngine;

public class ScoreAIUpdater : MonoBehaviour
{
    public void Start()
    {
        GameController.GETInstance().ScoreAIUpdater = this;
    }

    public void UpdateScore(int newScore)
    {
        GetComponent<TextMeshProUGUI>().text = newScore.ToString();
    }
}