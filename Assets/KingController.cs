﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KingController : MonoBehaviour
{
    private const int MAXMoveSteps = 50;

    private static readonly List<RelativeCoordinates> AvailableMoves = new List<RelativeCoordinates>
    {
        new RelativeCoordinates {Horizontal = 0, Vertical = 1},
        new RelativeCoordinates {Horizontal = 1, Vertical = 1},
        new RelativeCoordinates {Horizontal = 1, Vertical = 0}
    };

    private RelativeCoordinates _coordinates;
    private bool _isMoving;

    public void Start()
    {
        GameController.GETInstance().KingController = this;
        _coordinates = CoordinateTransformer.transform(gameObject.transform.position);
    }

    public void Move(RelativeCoordinates cellTo, PlayerType currentPlayer)
    {
        StartCoroutine(CyclicMove(cellTo, currentPlayer));
    }

    private IEnumerator CyclicMove(RelativeCoordinates cellTo, PlayerType currentPlayer)
    {
        if (_isMoving)
        {
            yield break;
        }

        _isMoving = true;
        RelativeCoordinates moveVector;
        try
        {
            moveVector = GETMoveVector(cellTo);
        }
        catch (IncorrectMoveException e)
        {
            GameController.GETInstance().ShowError(e.Message);
            yield break;
        }

        var moveMessagePrefix = currentPlayer == PlayerType.ArtificialIntelligence ? "Компьютер" : "Игрок";
        GameController.GETInstance().ShowMessage(moveMessagePrefix + " ходит в клетку " + cellTo.ToMessage());

        for (var i = 0; i < MAXMoveSteps; i++)
        {
            MoveStep(moveVector, i);
            yield return new WaitForSeconds(1f / MAXMoveSteps);
        }
    }

    private RelativeCoordinates GETMoveVector(RelativeCoordinates cellTo)
    {
        var diff = cellTo - _coordinates;
        if (AvailableMoves.Contains(diff))
        {
            return diff;
        }

        _isMoving = false;
        throw new IncorrectMoveException();
    }

    private void MoveStep(RelativeCoordinates moveVector, int stepIndex)
    {
        var newPosition = gameObject.transform.position;
        newPosition.x += (float) moveVector.Horizontal * GameController.FieldSize / MAXMoveSteps;
        newPosition.z += (float) moveVector.Vertical * GameController.FieldSize / MAXMoveSteps;
        // ReSharper disable once Unity.InefficientPropertyAccess
        gameObject.transform.position = newPosition;
        if (stepIndex != MAXMoveSteps - 1)
        {
            return;
        }

        _coordinates = CoordinateTransformer.transform(newPosition);
        GameController.GETInstance().KingMoveEvent
            .Publish(_coordinates);
        _isMoving = false;
    }
}