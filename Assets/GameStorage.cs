public class GameStorage
{
    public PlayerType CurrentPlayer { set; get; }
    public PlayerType InitialRoundPlayer { set; get; }
    public int[,] CellPoints { get; set; }
    public int RoundNumber { get; set; } = -1;
    public int AIScore { get; set; }
    public int PlayerScore { get; set; }
    public int TotalPlayerScore { get; set; }
    public int TotalAIScore { get; set; }
    public int MatchPlayerScore { get; set; }
    public int MatchAIScore { get; set; }

    public void InitializeMatch(PlayerType initialRoundPlayer)
    {
        InitialRoundPlayer = initialRoundPlayer;
        CurrentPlayer = initialRoundPlayer;
        RoundNumber = 0;

        MatchPlayerScore = 0;
        MatchAIScore = 0;
        PlayerScore = 0;
        AIScore = 0;
    }

    public void InitializeRound()
    {
        PlayerScore = 0;
        AIScore = 0;
        RoundNumber++;

        if (InitialRoundPlayer == PlayerType.LivePlayer)
        {
            InitialRoundPlayer = PlayerType.ArtificialIntelligence;
            CurrentPlayer = PlayerType.ArtificialIntelligence;
        }
        else
        {
            InitialRoundPlayer = PlayerType.LivePlayer;
            CurrentPlayer = PlayerType.LivePlayer;
        }
    }

    public void FinishRound()
    {
        MatchPlayerScore += PlayerScore;
        MatchAIScore += AIScore;
        TotalPlayerScore += PlayerScore;
        TotalAIScore += AIScore;
    }

    public GameScore GetGameScore()
    {
        return new GameScore
        {
            RoundNumber = RoundNumber + 1,

            RoundAIScore = AIScore,
            RoundPlayerScore = PlayerScore,

            MatchAIScore = MatchAIScore,
            MatchPlayerScore = MatchPlayerScore,

            TotalAIScore = TotalAIScore,
            TotalPlayerScore = TotalPlayerScore
        };
    }
}