using Events;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameController
{
    public const int FieldSize = 10;
    public const int RoundNumber = 2;

    public KingMoveEvent KingMoveEvent { get; } = new KingMoveEvent();
    public KingController KingController { get; set; }
    public MessageController MessageController { private get; set; }
    public ArtificialIntelligence ArtificialIntelligence { get; set; }
    public ScorePlayerUpdater ScorePlayerUpdater { get; set; }
    public ScoreAIUpdater ScoreAIUpdater { get; set; }

    private static volatile GameController _instance;
    private static object _lockObject = new Object();

    private static readonly GameStorage GameStorage = new GameStorage();

    private GameController()
    {
    }

    public static GameController GETInstance()
    {
        var local = _instance;

        if (local == null)
        {
            lock (_lockObject)
            {
                if (local == null)
                {
                    local = _instance = new GameController();
                }
            }
        }

        return local;
    }

    public void LivePlayerMove(RelativeCoordinates coordinates)
    {
        if (GameStorage.CurrentPlayer == PlayerType.ArtificialIntelligence)
        {
            ShowError("Дождитесь своего хода");
            return;
        }

        KingController.Move(coordinates, GameStorage.CurrentPlayer);
    }

    public void ArtificialIntelligenceMove(RelativeCoordinates coordinates)
    {
        KingController.Move(coordinates, GameStorage.CurrentPlayer);
    }

    public void MoveFinished(RelativeCoordinates coordinates)
    {
        UpdateScore(coordinates);
        ChangePlayer();
        ProcessRoundFinishIfNeeded(coordinates);

        if (GameStorage.CurrentPlayer == PlayerType.ArtificialIntelligence)
        {
            ArtificialIntelligence.MakeMove(coordinates);
        }
    }

    private void UpdateScore(RelativeCoordinates coordinates)
    {
        if (GameStorage.CurrentPlayer == PlayerType.ArtificialIntelligence)
        {
            GameStorage.AIScore += GameStorage.CellPoints[coordinates.Horizontal, coordinates.Vertical];
            ScoreAIUpdater.UpdateScore(GameStorage.AIScore);
        }
        else
        {
            GameStorage.PlayerScore += GameStorage.CellPoints[coordinates.Horizontal, coordinates.Vertical];
            ScorePlayerUpdater.UpdateScore(GameStorage.PlayerScore);
        }
    }

    private static void ChangePlayer()
    {
        GameStorage.CurrentPlayer = GameStorage.CurrentPlayer == PlayerType.LivePlayer
            ? PlayerType.ArtificialIntelligence
            : PlayerType.LivePlayer;
    }

    private static void ProcessRoundFinishIfNeeded(RelativeCoordinates coordinates)
    {
        if (coordinates.Horizontal != FieldSize - 1 || coordinates.Vertical != FieldSize - 1)
        {
            return;
        }

        GameStorage.FinishRound();
        SceneManager.LoadScene(GameStorage.RoundNumber < RoundNumber - 1 ? "IntermediateScene" : "FinalScene");
    }

    public void ShowMessage(string message)
    {
        MessageController.ShowMessage(message, Color.blue);
    }

    public void ShowError(string message)
    {
        MessageController.ShowMessage(message, Color.red);
    }

    public static PlayerType GetCurrentPlayer()
    {
        return GameStorage.CurrentPlayer;
    }

    public static void InitializeMatch(PlayerType player)
    {
        GameStorage.InitializeMatch(player);
    }

    public static void InitializeRound()
    {
        GameStorage.InitializeRound();
    }

    public static void SetCellPoints(int[,] cellPoints)
    {
        GameStorage.CellPoints = cellPoints;
    }

    public static GameScore GetGameScore()
    {
        return GameStorage.GetGameScore();
    }
}