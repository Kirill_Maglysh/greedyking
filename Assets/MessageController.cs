﻿using System.Collections;
using TMPro;
using UnityEngine;

public class MessageController : MonoBehaviour
{
    private const int FadeSteps = 50;
    private const float FadeStepInterval = 0.1f;

    private float _ligthness;
    private bool _isShowed;
    private Coroutine _fadeCoroutine;

    public void Start()
    {
        GameController.GETInstance().MessageController = this;
        ShowInitialMoveMessage();
    }

    private void ShowInitialMoveMessage()
    {
        if (GameController.GetCurrentPlayer() == PlayerType.LivePlayer)
        {
            ShowMessage("Ваш ход...", Color.blue);
        }
    }

    public void FixedUpdate()
    {
        if (!_isShowed)
        {
            return;
        }

        var color = gameObject.GetComponent<TextMeshProUGUI>().color;
        color.a = _ligthness;
        gameObject.GetComponent<TextMeshProUGUI>().color = color;

        if (_ligthness == 0)
        {
            _isShowed = false;
        }
    }

    public void ShowMessage(string message, Color color)
    {
        if (_fadeCoroutine != null)
        {
            StopCoroutine(_fadeCoroutine);
        }

        gameObject.GetComponent<TextMeshProUGUI>().text = message;
        gameObject.GetComponent<TextMeshProUGUI>().color = color;

        _fadeCoroutine = StartCoroutine(Fade());
    }

    private IEnumerator Fade()
    {
        _isShowed = true;
        _ligthness = 1;
        float stepLightness = 0;
        while (stepLightness <= FadeSteps)
        {
            if (stepLightness > FadeSteps / 2)
            {
                _ligthness -= 2f / FadeSteps;
            }

            stepLightness++;
            yield return new WaitForSeconds(FadeStepInterval);
        }
    }
}