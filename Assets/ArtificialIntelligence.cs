using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArtificialIntelligence : MonoBehaviour
{
    private CellStrategy[,] field = new CellStrategy[GameController.FieldSize, GameController.FieldSize];

    private void Start()
    {
        GameController.GETInstance().ArtificialIntelligence = this;
    }

    public void Initialize(int[,] cellPoints)
    {
        FillEndCellStrategy(cellPoints);
        FillTopRow(cellPoints);
        FillRightColumn(cellPoints);
        FillRemainingCells(cellPoints);
    }

    private void FillEndCellStrategy(int[,] cellPoints)
    {
        const int horizontal = GameController.FieldSize - 1;
        const int vertical = GameController.FieldSize - 1;
        
        var originalCellCoordinates = new RelativeCoordinates
        {
            Horizontal = horizontal, 
            Vertical = vertical
        };
        var cellPoint = cellPoints[horizontal, vertical];

        field[horizontal, vertical] =
            new CellStrategy
                {
                    MaximumPlayerGamePoints = cellPoint,
                    MaximumOpponentGamePoints = 0,
                    OriginalCellCoordinates = originalCellCoordinates,
                    OptimalMoveCellCoordinates = null
                }
                .CalcDifference();
    }

    private void FillTopRow(int[,] cellPoints)
    {
        const int topHorizontal = GameController.FieldSize - 1;

        for (var i = GameController.FieldSize - 2; i >= 0; i--)
        {
            var nextVertical = i + 1;
            var maximumPlayerGamePoints = cellPoints[topHorizontal, i] +
                                          field[topHorizontal, nextVertical].MaximumOpponentGamePoints;
            var maximumOpponentGamePoints = field[topHorizontal, nextVertical].MaximumPlayerGamePoints;

            var originalCellCoordinates = new RelativeCoordinates
            {
                Horizontal = topHorizontal,
                Vertical = i
            };

            var optimalMoveCellCoordinates = new RelativeCoordinates
            {
                Horizontal = topHorizontal,
                Vertical = nextVertical
            };

            field[topHorizontal, i] = new CellStrategy
                {
                    MaximumPlayerGamePoints = maximumPlayerGamePoints,
                    MaximumOpponentGamePoints = maximumOpponentGamePoints,
                    OriginalCellCoordinates = originalCellCoordinates,
                    OptimalMoveCellCoordinates = optimalMoveCellCoordinates
                }
                .CalcDifference();
        }
    }

    private void FillRightColumn(int[,] cellPoints)
    {
        const int rightVertical = GameController.FieldSize - 1;

        for (var i = GameController.FieldSize - 2; i >= 0; i--)
        {
            var nextHorizontal = i + 1;
            var maximumPlayerGamePoints = cellPoints[i, rightVertical] +
                                          field[nextHorizontal, rightVertical].MaximumOpponentGamePoints;
            var maximumOpponentGamePoints = field[nextHorizontal, rightVertical].MaximumPlayerGamePoints;

            var originalCellCoordinates = new RelativeCoordinates
            {
                Horizontal = i,
                Vertical = rightVertical
            };

            var optimalMoveCellCoordinates = new RelativeCoordinates
            {
                Horizontal = nextHorizontal,
                Vertical = rightVertical
            };

            field[i, rightVertical] = new CellStrategy
                {
                    MaximumPlayerGamePoints = maximumPlayerGamePoints,
                    MaximumOpponentGamePoints = maximumOpponentGamePoints,
                    OriginalCellCoordinates = originalCellCoordinates,
                    OptimalMoveCellCoordinates = optimalMoveCellCoordinates
                }
                .CalcDifference();
        }
    }

    private void FillRemainingCells(int[,] cellPoints)
    {
        for (var i = GameController.FieldSize - 2; i >= 0; i--)
        {
            for (var j = GameController.FieldSize - 2; j >= 0; j--)
            {
                var moveVariants = new List<CellStrategy> {field[i + 1, j], field[i, j + 1], field[i + 1, j + 1]};
                var optimalMoveCell = CalculateBestVariant(moveVariants);

                var maximumPlayerGamePoints = optimalMoveCell.MaximumOpponentGamePoints + cellPoints[i, j];
                var originalCellCoordinates = new RelativeCoordinates
                {
                    Horizontal = i,
                    Vertical = j
                };

                field[i, j] = new CellStrategy
                    {
                        MaximumPlayerGamePoints = maximumPlayerGamePoints,
                        MaximumOpponentGamePoints = optimalMoveCell.MaximumPlayerGamePoints,
                        OriginalCellCoordinates = originalCellCoordinates,
                        OptimalMoveCellCoordinates = optimalMoveCell.OriginalCellCoordinates
                    }
                    .CalcDifference();
            }
        }
    }

    private static CellStrategy CalculateBestVariant(List<CellStrategy> moveVariants)
    {
        moveVariants.Sort();
        return moveVariants[moveVariants.Count - 1];
    }

    public void MakeMove(RelativeCoordinates kingCoordinates)
    {
        StartCoroutine(FakeWaitingMoveCalculation(kingCoordinates));
    }

    private IEnumerator FakeWaitingMoveCalculation(RelativeCoordinates kingCoordinates)
    {
        yield return new WaitForSeconds(1);
        GameController.GETInstance()
            .ArtificialIntelligenceMove(field[kingCoordinates.Horizontal, kingCoordinates.Vertical]
                .OptimalMoveCellCoordinates);
    }

    private class CellStrategy : IComparable<CellStrategy>
    {
        private int _maximumGameDifference;
        public int MaximumPlayerGamePoints { get; set; }
        public int MaximumOpponentGamePoints { get; set; }

        public RelativeCoordinates OriginalCellCoordinates { get; set; }
        public RelativeCoordinates OptimalMoveCellCoordinates;

        public CellStrategy CalcDifference()
        {
            _maximumGameDifference = MaximumPlayerGamePoints - MaximumOpponentGamePoints;
            return this;
        }


        public int CompareTo(CellStrategy other)
        {
            if (ReferenceEquals(this, other)) return 0;
            if (ReferenceEquals(null, other)) return 1;

            var differenceComparison = _maximumGameDifference.CompareTo(other._maximumGameDifference);
            if (differenceComparison != 0) return differenceComparison;

            var pointsComparison = MaximumPlayerGamePoints.CompareTo(other.MaximumPlayerGamePoints);
            return pointsComparison != 0
                ? pointsComparison
                : MaximumOpponentGamePoints.CompareTo(other.MaximumOpponentGamePoints);
        }
    }
}