public class RelativeCoordinates
{
    private readonly string[] _horizontalLabels = {"A", "B", "C", "D", "E", "F", "G", "H", "J", "K"};

    public int Horizontal { get; set; }
    public int Vertical { get; set; }

    public static RelativeCoordinates operator -(RelativeCoordinates coordinates1, RelativeCoordinates coordinates2)
    {
        return new RelativeCoordinates()
        {
            Horizontal = coordinates1.Horizontal - coordinates2.Horizontal,
            Vertical = coordinates1.Vertical - coordinates2.Vertical
        };
    }

    public static bool operator ==(RelativeCoordinates coordinates1, RelativeCoordinates coordinates2)
    {
        return !(coordinates2 is null) && !(coordinates1 is null) &&
               coordinates1.Horizontal == coordinates2.Horizontal && coordinates1.Vertical == coordinates2.Vertical;
    }

    public static bool operator !=(RelativeCoordinates coordinates1, RelativeCoordinates coordinates2)
    {
        return !(coordinates1 == coordinates2);
    }

    public override bool Equals(object obj)
    {
        return this == (RelativeCoordinates) obj;
    }

    public override int GetHashCode()
    {
        return Horizontal * 31 + Vertical;
    }

    public string ToMessage()
    {
        return _horizontalLabels[Vertical] + Horizontal;
    }
}